# ATTEST

Utilitaire fonctionnant dans un terminal de commande qui génère une  
attestation de déplacement dérogatoire en application des mesures  
générales nécessaires pour faire face à l'épidémie de covid-19 dans  
le cadre de l'état d'urgence sanitaire, au format texte pour le motif  
indiqué et l'envoie par e-mail à l'adresse choisie par l'utilisateur  
avec, au choix, un QRCode inclus en pièce-jointe.

# DÉPENDENCES

bash, coreutils, qrencode, s-nail, nano (optionnel)

# AVERTISSEMENT

Ce programme est un logiciel libre, fourni tel quel, sans AUCUNE GARANTIE.  
Voir le fichier LICENSE inclus dans ce dépôt.

# INSTALLATION

Cloner le dépot `git clone https://gitlab.com/teegre/attest.git`, puis :  
```
cd attest
./install.sh
```

# UTILISATION

**attest** [**qr**] [**bavard**] UTILISATEUR [MOTIF [HEURE]]  
**attest** **profil** UTILISATEUR  
**attest** **profils**  
**attest** **efface** UTILISATEUR  
**attest** **motifs**  
**attest** **aide**

# OPTIONS

**qr**  
    - Génère un QRCode qui sera joint à l'e-mail.

**bavard**  
    - Affiche davantage d'informations lorsqu'un e-mail est envoyé.

**profil** UTILISATEUR  
    - Crée ou modifie un profil pour l'utilisateur donné.  

**efface** UTILISATEUR  
    - Supprime un profil utilisateur.  

**profils**  
    - Affiche la liste des profils existants.  

**motifs**  
    - Affiche la liste des motifs de déplacement.  

**aide**  
    - Affiche cet écran d'aide.

## GLOSSAIRE

### QRCode
Le QRCode inclus dans l'e-mail encapsule toutes les données du profil utilisateur choisi lors de la génération de l'attestation  
de déplacement dérogatoire, ainsi que la date et l'heure de sortie, la date et l'heure de création de ladite attestation, et  
pour finir le motif de la sortie sous forme abrégée.

### UTILISATEUR
Nom de l'utilisateur tel que stipulé lors de la création d'un profil.

### MOTIF
Numéro de motif tel qu'indiqué par l'option **motifs**.  
Si le paramètre est omis, le motif renseigné dans le  profil de l'utilisateur est utilisé.

### HEURE
Heure de début du déplacement prévu.  
Si elle n'est pas définie, **attest** utilise l'heure courante + 5 minutes.

# CONFIGURATION

L'option `email` permet d'éditer le fichier de configuration de la boîte mail pour l'envoi, *.email*.

Voici les paramètres dont **attest** a besoin :

  * `adresse_email`

    Adresse qui sera utilisée pour l'envoi des attestations.

  * `serveur_smtp`

    Adresse du serveur d'envoi (par ex : smtp.xooglx.com).

  * `protocole`

    Peut être smtp, smtps ou submission.<br>
    Si le paramètre est laissé vide, **attest** se base sur le numéro de port afin de le deviner.

  * `port`

    Généralement 465 ou 587.

  * `mot_de_passe`

    Afin d'éviter d'avoir à écrire le mot de passe en clair dans le fichier de configuration,  
    il est possible d'utiliser une commande (pour lancer un gestionnaire de mots de passe),  
    ou d'entrer le mot de passe encodé en base64.


## SYNTAXE POUR LES MOTS DE PASSE

COMMANDE :

  **$(COMMANDE)**
  
  Exemple : `mot_de_passe=$(pass mail/xooglx)`

ENCODAGE EN BASE64 :

  **@(BASE64)**

  Pour encoder un mot de passe en base64 : `echo "mot_de_passe" | base64`

  Copier le résultat dans le fichier *.email* : `mot_de_passe=@(bW90X2RlX3Bhc3NlCg==)`


# PROFILS

Avant d'envoyer une attestation de déplacement dérogatoire,  
un profil utilisateur doit être créé avec l'option `profil`.  
Cette option permet également de modifier un profil existant.

## FICHIERS .profil

Un fichier *.profil* contient les informations de l'utilisateur, à savoir :

 * Civilité
 * Nom
 * Prénom
 * Date de naissance
 * Lieu de naissance
 * Adresse (sur une ligne : n° voie - code postal ville)
 * E-mail
 * Motif de déplacement par défaut

Celle-ci sont nécessaires pour générer l'attestation.

_Important : l'adresse e-mail choisie doit être configurée également_  
_sur le téléphone mobile de l'utilisateur._

# EXEMPLE

  * Création d'un utilisateur :

    `attest profil jessica`

  * Remplir le profil utilisateur :

```
      civilite=Mme
      nom=Trizet
      prenom=Jessica
      naissance=01/01/1980
      lieu=Ici
      adresse=1 rue de la Route - 12345 LaVille
      email=jessica@nomdedomaine.com
      motif=1
```

  * Générer une attestation

    `attest jessica`

  * Pour utiliser un autre motif à une heure prédéfinie :

    `attest jessica 5 "16:30"`

  * Pour inclure un QRCode à l'e-mail

    `attest qr jessica`

  * Affichier la liste des profils

    `attest profils`

  * Effacer un profil utilisateur

    `attest efface jessica`

