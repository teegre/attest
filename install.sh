#! /usr/bin/env bash

BINDIR="$HOME/.local/bin"
CONFIGDIR="$HOME/.config/attest"

echo "--- début de l'installation."

mkdir -p     "$CONFIGDIR" 2> /dev/null && echo "::: $CONFIGDIR créé."

cp modèle    "$CONFIGDIR" && echo "::: fichier modèle  copié."
cp motifs    "$CONFIGDIR" && echo "::: fichier motifs  copié."
cp .profil   "$CONFIGDIR" && echo "::: fichier .profil copié."
cp -n .email "$CONFIGDIR" && echo "::: fichier .email  copié."
chmod 600 "$CONFIGDIR/.email" && echo "::: permissions définies (.email -> 600)."
cp attest    "$BINDIR"    && echo "::: attest copié vers $BINDIR."

cp attest.1  "$HOME/.local/share/man/man1" && echo "::: page de manuel copiée."

echo "--- installation terminée."

