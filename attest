#! /usr/bin/env bash

# ATTEST (C) 2020 Teegre
#
# Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier
# suivant les termes de la GNU General Public License telle que publiée par la 
# Free Software Foundation ; soit la version 3 de la licence, soit
# (à votre gré) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; sans même la garantie tacite de
# QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
# Consultez la GNU General Public License pour plus de détails.
# Vous devez avoir reçu une copie de la GNU General Public License
# en même temps que ce programme ;
# si ce n'est pas le cas, consultez <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

CONFIGDIR="$HOME/.config/attest"
MODELE="$CONFIGDIR/modèle"
MOTIFS="$CONFIGDIR/motifs"
EMAILCONF="$CONFIGDIR/.email"

__err() {
  # Affiche un message.

  local genre categ msg
  case $1 in
    A) genre="[!] " ;; # Avertissement
    E) genre='|!| ' ;; # Erreur
    M) genre="::: " ;; # Message simple
  esac
  shift
  [[ $1 != "-" ]] && categ="$1"
  shift
  msg="$1"
  [[ $categ ]] || {
    ( >&2 echo -e "${genre}$msg" )
    return 0
  }
  ( >&2 echo -e "${genre}|${categ} : $msg" )
    return 0
}

lire_param() {
  # Retourne la valeur d'un paramètre dans un fichier de configuration
  # ou le profil utilisateur.
  #
  # __f fichier
  # __p paramètre
  # __l ligne du fichier
  # __r expression rationnelle

  local __f __p __l __r
  __p="$1"
  __f="${2:-$PROFIL}"
  [[ $__p && $__f ]] || { echo "null"; return 1; }
  __r="^[[:space:]]*${__p%=*}[[:space:]]*=[[:space:]]*(.*)$"
  while read -r __l; do
    # On ignore les commentaires...
    [[ $__r =~ ^#.*$ ]] && continue
    if [[ $__l =~ $__r ]]; then
      if [[ ! ${BASH_REMATCH[1]} ]]; then
        echo "null"
        return 1
      else
        echo "${BASH_REMATCH[1]}"
        return 0
      fi
    fi
  done < "$__f"

  echo "null"
  return 1
}

profil() {
  # Crée ou modifie un profil utilisateur.

  [[ $1 ]] || { __err E profil "nom de profil non spécifié."; return 1; }
  local f="$CONFIGDIR/$1.profil"
  [[ -a "$f" ]] || {
    cp "$CONFIGDIR/.profil" "$f" || { __err E .profil "fichier .profil introuvable."; exit 1; }
    chmod 600 "$f"
    local nouveau=1;
  }
  [[ $nouveau ]] && {
    local time1 time2
    time1="$(stat -c "%Y" "$f")"
  }
  ${EDITOR:-nano} "$f" 2> /dev/null || { __err E .profil "pas d'éditeur défini (EDITOR)."; return 1; }
  [[ $nouveau ]] && {
    time2="$(stat -c "%Y" "$f")"
    [[ $time1 == "$time2" ]] && {
      rm "$f"
      __err M .profil "aucun changement - fichier supprimé."
      return 0
    }
  }
  __err M - "OK."
}

profils() {
  # Affiche la liste des profils.

  local __p __c=0
  while read -r __p; do
    [[ $__p =~ ^.*/\.profil$ ]] && continue
    [[ $__p =~ ^.*/(.*)\.profil$ ]] && {
      __err M - "${BASH_REMATCH[1]}"
      ((__c++))
    }
  done < <( find "$CONFIGDIR" -name "*.profil" )
  ((__c>1)) && __err M - "\n-- $__c profils trouvés."
  ((__c<2)) && __err M - "\n-- $__c profil trouvé."
}

efface() {
  # Efface un utilisateur.

  [[ $1 ]] || { __err E efface "nom de profil manquant." ; return 1; }
  local f="$CONFIGDIR/$1.profil"
  [[ -a $f ]] || { __err E efface "profil inexistant." ; return 1; }
  rm -i "$f" || { __err A efface "annulé."; return 1; }
  __err M efface "profil supprimé."
}

envoi() {
  # Envoie une attestation par e-mail.

# shellcheck disable=2086
{
  gen_mailrc || return 1

  local err

  if [[ $QR ]]; then
    echo -e "$ATTESTATION" | mail \
        ${OPTIONS[*]} \
        -s "[$PRENOM] Attestation du $DATE" \
        -a "$QRCODE" \
        "$EMAIL" || err=1
  else
    echo -e "$ATTESTATION" | mail \
        ${OPTIONS[*]} \
        -s "[$PRENOM] Attestation du $DATE" \
        "$EMAIL" || err=1
  fi

  [[ -d $TEMPDIR ]] && rm -rf "$TEMPDIR"

  [[ $BV ]] && echo "---"

  [[ $err ]] && return 1

  return 0
}
}

email_conf() {
  # Ouvre le fichier de configuration de la boîte mail d'envoi.

  [[ -a "$EMAILCONF" ]] && {
    ${EDITOR:-nano} "$EMAILCONF" 2> /dev/null || { __err E .email "pas d'éditeur défini (EDITOR). "; return 1; }
    __err M - "OK."
    return 0
  }
  __err E .email "fichier de configuration introuvable."
  return 1
}

dir_temp() {
  # Crée un répertoire temporaire.

  [[ $TEMPDIR ]] && return 0
  TEMPDIR="$(mktemp -dq)" || {
    __err E - "création du répertoire temporaire impossible."
    return 1
  }
}

# N'étant pas parvenu à ajouter une pièce-jointe du QRCode aux e-mails
# à l'aide de 'xdg-email' et après de nombreuses recherches, la solution
# me paraissant la plus appropriée est d'utiliser la commande 'mail'.
# Mais pour cela nous avons besoin de passer des options sur la ligne de
# commande.
# Cependant, si l'on trouve un fichier .mailrc dans le système, alors 
# la fonction ci-dessous ne fait rien. Sinon, elle crée une variable 
# contenant les options de 'mail' avec les informations stockées 
# dans le fichier :  ~/.config/attest/.email
#
#     PARAM         : EXAMPLE / DESC.
#     adresse_email : utilisateur@xooglx.com
#     serveur_smtp  : smtp.xmaix.com
#     protocole     : smtps
#     port          : 465
#     mot_de_passe  : possibilité d'entrer une commande au lieu du
#                   | mot de passe en dur.
#                   | Ex : $(pass mail/xooglx)
#                   | Il est également possible de renseigner un
#                   | mot de passe encodé en base64.
#                   | Ex : @(bW90ZGVwYXNzZQo=)

gen_mailrc() {
    # Génère les options de configuration du compte e-mail pour l'envoi.
    # Ou pas...

    [[ -s "$HOME/.mailrc" ]] && return 0

    local err utilisateur email smtp protocole motdepasse port

    # E-mail pour l'envoi.
    email="$(lire_param "adresse_email" "$EMAILCONF")" || {
      __err E .email "adresse e-mail pour l'envoi non renseignée."
      err=1
    }
    
    # Serveur smtp.
    smtp="$(lire_param "serveur_smtp" "$EMAILCONF")" || {
      __err E .email "adresse du serveur smtp non renseignée."
      err=1
    }
    
    # Port.
    port="$(lire_param "port" "$EMAILCONF")" || {
      __err A .email "port non renseigné."
      port=587
      __err M - "$port sera utilisé par défaut."
    }

    # Protocole.
    protocole="$(lire_param "protocole" "$EMAILCONF")" || {
      __err A .email "protocole non renseigné."
      [[ $port == 465 ]] && protocole="smtps" || protocole="smtp"
      __err M - "$protocole sera utilisé par défaut."
    }

    # Mot de passe.
    motdepasse="$(lire_param "mot_de_passe" "$EMAILCONF")" || {
      __err E .email "mot de passe non renseigné."
      err=1
    }

    if [[ $email =~ ^(.+)@.+\..+$ ]]; then
      utilisateur="${BASH_REMATCH[1]}"
    else
      __err E .email "adresse e-mail invalide."
      err=1
    fi

    [[ $err ]] && return 1

    if [[ $motdepasse =~ ^.\((.*)\)$ ]]; then
      [[ ${BASH_REMATCH[1]} ]] || {
        __err E .email "mot de passe absent."
        return 1;
      }
    fi

    if [[ $motdepasse =~ ^\$\((.+)\)$ ]]; then
      local cmd="${BASH_REMATCH[1]}"
      motdepasse="$($cmd)" 2> /dev/null  || {
        __err E .email "commande invalide."
        return 1
      }
    elif [[ $motdepasse =~ ^\$\(.+[^\)]$ ]]; then
      __err E .email "commande de mot de passe invalide."
      return 1
    elif [[ $motdepasse =~ ^@\((.+)\)$ ]]; then
      motdepasse="$(echo "${BASH_REMATCH[1]}" | base64 -d)"
    elif [[ $motdepasse =~ ^@\(.+[^\)]$ ]]; then
      __err E .email "mot de passe base64, syntaxe erronnée."
      return 1
    fi

    dir_temp || return 1

    OPTIONS=( \
        "-S v15-compat"                                            \
        "-S tls-verify=strict"                                     \
        "-S tls-ca-file=/etc/ssl/certs/ca-certificates.crt"        \
        "-S tls-ca-no-defaults"                                    \
        "-S tls-config-pairs=Protocol=-ALL\,+TLSv1.2"              \
        "-S smtp-use-starttls"                                     \
        "-S smtp-auth=login"                                       \
        "-S sendwait"                                              \
        "-S folder=$TEMPDIR"                                       \
        "-S from=$email"                                           \
        "-S mta=$protocole://$utilisateur:$motdepasse@$smtp:$port" \
    ) 

    [[ $BV ]] && OPTIONS+=( "-v" )
    echo "---"
}

gen_qrcode() {
  # Génère un QRCode.

  dir_temp || return 1

  local donnees
  local motifs=(     \
    "Travail"        \
    "Achats"         \
    "Sante"          \
    "Famille"        \
    "Handicap"       \
    "Sport/Animaux"  \
    "Convocation"    \
    "Missions"       \
    "Enfants"        \
  )

  local D d T
  
  D="$(date "+%d/%m/%Y")"
  d="$(date "+%Y%m%d%H%M%S")"
  T="$(date "+%H:%M")"

  QRCODE="$TEMPDIR/qrcode-${d}.png"
  
  donnees="Creation : $D, $T\nNom : $NOM\nPrenom : $PRENOM\nNaissance : $NAISSANCE a $LIEU\nAdresse : $ADRESSE\nSortie : $DATE, $HEURE\nMotifs : ${motifs[$INDEX]}\n"

  echo -e "$donnees" | qrencode -o "$QRCODE" || {
    __err E - "impossible de générer le QRCode."
    __err M - "L'e-mail sera envoyé sans pièce-jointe."
    unset QR QRCODE
    return 1
  }
  echo "---"
}

liste() {
  # Affiche la liste des motifs de déplacement.

  local __m
  while read -r __m; do
    echo -e "$__m"
  done < "$MOTIFS"
}

aide() {
# Affiche un écran d'aide.

cat << 'EOB' >&2
Utilitaire fonctionnant dans un terminal de commande qui génère une
attestation de déplacement dérogatoire en application des mesures
générales nécessaires pour faire face à l'épidémie de covid-19 dans
le cadre de l'état d'urgence sanitaire, au format texte pour le motif
indiqué et l'envoie par e-mail à l'adresse choisie par l'utilisateur
avec, au choix, un QRCode inclus en pièce-jointe.

Synopsis :

attest [OPTIONS] UTILISATEUR [MOTIF [HEURE]]

Utilisation :
attest [qr] [bavard] UTILISATEUR [MOTIF [HEURE]]
attest profil UTILISATEUR
attest profils
attest efface UTILISATEUR
attest motifs
attest aide

Options :
qr                 - Génère un QRCode et le joint à l'e-mail.
bavard             - Affiche des informations supplémentaires lors de l'envoi de l'e-mail.
profil UTILISATEUR - Crée ou modifie un profil pour l'utilisateur donné.
profils            - Affiche la liste des profils.
efface UTILISATEUR - Supprime un profil utilisateur.
email              - Ouvre le fichier de la boîte mail d'envoi.
motifs             - Affiche la liste des motifs de déplacement.
aide               - Affiche cet écran d'aide.

Glossaire :
- QRCode :
Le QRCode inclus dans l'e-mail encapsule toutes les données du
profil utilisateur choisi lors de la génération de l'attestation
de déplacement dérogatoire, ainsi que la date et l'heure de sortie,
la date et l'heure de la création de ladite attestation, et pour
finir le motif de la sortie sous forme abrégée.

- UTILISATEUR :
Nom de l'utilisateur tel que stipulé lors de la création d'un profil.

- MOTIF :
Numéro de motif tel qu'indiqué par l'option motifs.
Si le paramètre est omis, le motif renseigné dans le
profil de l'utilisateur est utilisé.

- HEURE :
Heure de début du déplacement prévu.
Si elle n'est pas définie, attest utilise l'heure courante + 5 minutes.

Pour plus d'information, consulter : man attest

EOB
}

echo "attest - copyright (c) 2020 Teegre"
echo "Ce programme est fourni SANS AUCUNE GARANTIE."
echo -e "Ce programme est un logiciel libre, librement redistribuable.\n"

# Vérifier si tous les fichiers nécessaires sont présents.
[[ -a "$MOTIFS" ]] || { __err E - "fichier 'motifs' non trouvé."; exit 1; }
[[ -a "$MODELE" ]] || { __err E - "fichier 'modèle' non trouvé."; exit 1; }

# Lire les options de la ligne de commande.
case $1 in
  profil ) profil "$2"; exit $? ;;
  profils) profils; exit 0 ;;
  efface ) efface "$2" || exit 1; exit 0 ;;
  email  ) email_conf || exit 1; exit 0 ;;
  motifs ) liste; exit 0 ;;
  aide   ) aide; exit 0 ;;
  ""     ) __err E - "entrez '$(basename "$0") aide' pour plus d'informations"; exit 1
esac

# Doit-on générer le qrcode?
[[ $1 == "qr" ]] && { QR=1; shift; }

# Mode bavard ?
[[ $1 == "bavard" ]] && { BV=1; shift; }

# Avons-nous un utilisateur ?
[[ $1 ]] || { __err E - "pas de profil spécifié."; exit 1; }

# Récupérer le profil de l'utilisateur.
PROFIL="$CONFIGDIR/$1.profil"
shift

[[ -a "$PROFIL" ]] || {
  __err E - "pas de profil trouvé pour cet utilisateur."
  exit 1
}

# Index du motif de déplacement.
INDEX="${1:-"$(lire_param "motif")"}"
[[ $INDEX == "null" ]] && { __err E - "pas de motif de déplacement."; exit 1; }
[[ $1 ]] && shift
MOTIF="$(lire_param "$INDEX" "$MOTIFS")"
[[ $MOTIF ]] || { __err E - "motif de déplacement inconnu ($INDEX)"; exit 1; }

# Date et heure.
DATE="$(date "+%d %b %Y")"
HEURE="${1:-"$(date -d "now + 5 minutes" "+%H:%M")"}"

# Lecture du profil utilisateur.
CIV="$(lire_param "civilite")" || { __err E .profil "civilité manquante."; err=1; }
PRENOM="$(lire_param "prenom")" || { __err E .profil "prénom manquant."; err=1; }
NOM="$(lire_param "nom")" || { __err E .profil "nom manquant."; err=1; }
NAISSANCE="$(lire_param "naissance")" || { __err E .profil "date de naissance manquante."; err=1; }
LIEU="$(lire_param "lieu")" || { __err E .profil "lieu de naissance manquant."; err=1; }
ADRESSE="$(lire_param "adresse")" || { __err E .profil "adresse manquante."; err=1; }
EMAIL="$(lire_param "email")" || { __err E .profil "adresse e-mail manquante."; err=1; }

[[ $err ]] && exit 1

unset err

__err M - "Génération de l'attestation..."

ATTESTATION="$(< "$MODELE")"

# On remplace les champs entre crochets dans le fichier modèle
# par les informations récupérées dans le profil utilisateur.
ATTESTATION="${ATTESTATION//\[CIV\]/$CIV}"
ATTESTATION="${ATTESTATION//\[PRENOM\]/$PRENOM}"
ATTESTATION="${ATTESTATION//\[NOM\]/$NOM}"
ATTESTATION="${ATTESTATION//\[NAIS\]/$NAISSANCE}"
ATTESTATION="${ATTESTATION//\[VILNAIS\]/$LIEU}"
ATTESTATION="${ATTESTATION//\[ADR\]/$ADRESSE}"
ATTESTATION="${ATTESTATION//\[MOTIF\]/$MOTIF}"
ATTESTATION="${ATTESTATION//\[DATE\]/$DATE}"
ATTESTATION="${ATTESTATION//\[HEURE\]/$HEURE}"


[[ $QR ]] && { __err M - "Génération du QRCode..."; gen_qrcode; }

__err M - "Envoi de l'attestation du $DATE à $HEURE pour $PRENOM à $EMAIL..."

envoi && {
  __err M - "Le message a été envoyé."
  __err A - "Veuillez vérifier que vous l'avez reçu"
  __err A - "avant de quitter votre domicile."
  exit 0
}

__err E - "le message n'a pas pu être envoyé."
exit 1
